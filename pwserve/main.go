package main

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/yekimov/pwned"
)

type data struct {
	Hash  string
	Found int64
}

const strTpl = `<html>
<head>
<style type="text/css">
.green {
	color: green;
}
.red {
	color: red;
}
</style>
</head>

<body>

{{if .}}
<p class="{{if .Found}}red{{else}}green{{end}}">The password appeared {{.Found}} times.</p>
<p>Hash: {{.Hash}}</p>
{{end}}

<form method="POST">
<p>Enter password</p>
<p><input type="password" name="password"></p>
<input type="submit">

</form>

</body>
</html>`

var tpl *template.Template

func init() {
	var err error
	tpl = template.New("main")
	tpl, err = tpl.Parse(strTpl)
	if err != nil {
		panic(err)
	}
}

var filePath string

func findHash(password string) (*data, error) {
	sum := sha1.Sum([]byte(password))
	idx, err := pwned.FindHash(filePath, sum[:])
	return &data{
		Hash:  hex.EncodeToString(sum[:]),
		Found: idx,
	}, err
}

func handle(w http.ResponseWriter, r *http.Request) {
	var data interface{}
	switch r.Method {
	case "POST":
		err := r.ParseForm()
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("Can't parse input form: %v.", err)))
			return
		}
		password := r.FormValue("password")
		password = strings.TrimSpace(password)
		if password != "" {
			data, err = findHash(password)
			if err != nil {
				log.Println("failed find hash:", err)
			}
		}
	}
	w.WriteHeader(http.StatusOK)
	tpl.Execute(w, data)
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: pwned <dbFile>")
		os.Exit(1)
	}
	filePath = os.Args[1]
	http.HandleFunc("/", handle)
	panic(http.ListenAndServe(":8001", nil))
}
