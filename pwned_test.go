package pwned

import (
	"bufio"
	"encoding/binary"
	"encoding/hex"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"testing"
)

func TestFindHash(t *testing.T) {
	path, err := ioutil.TempDir("", "pwned_test")
	if err != nil {
		t.Fatal(err)
	}
	path = filepath.Join(path, "pwned.db")
	err = ConvertDatabase("pwned_test.txt", path, func(int) {}, func(err error) { t.Fatal(err) })
	defer os.Remove(path)
	if err != nil {
		t.Fatal(err)
	}
	file, err := os.Open("pwned_test.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	bfile := bufio.NewReader(file)
	line, ferr := bfile.ReadString('\n')
	i := 1
	for ferr == nil {
		line = strings.TrimSpace(line)
		parts := strings.Split(line, ":")
		hash, err := hex.DecodeString(parts[0])
		if err != nil {
			t.Fatal(err)
		}
		times, err := strconv.ParseInt(parts[1], 10, 64)
		if err != nil {
			t.Fatal(err)
		}
		times0, err := FindHash(path, hash)
		if err != nil {
			t.Fatal(err)
		}
		if times != times0 {
			t.Fatalf("line %d: times != times0, %d != %d", i, times, times0)
		}
		line, ferr = bfile.ReadString('\n')
		i++
	}
	if ferr != io.EOF {
		t.Fatal(err)
	}
	line = strings.TrimSpace(line)
	if line != "" {
		t.Fatalf("Last line not empty: %s", line)
	}
	buf := make([]byte, binary.MaxVarintLen64)
	for i := 0; i < 1000; i++ {
		n := binary.PutVarint(buf, int64(i))
		if n < 0 {
			panic("binary serialization failed")
		}
		times, err := FindHash(path, buf)
		if err != nil {
			t.Fatal(err)
		}
		if times != 0 {
			t.Fatalf("hash of %d found", i)
		}
	}
}
