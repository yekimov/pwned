package pwned

import (
	"bufio"
	"crypto/sha1"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"github.com/boltdb/bolt"
)

var sha1BucketName = []byte("sha1")

// FindHash is ...
func FindHash(dbPath string, hash []byte) (int64, error) {
	db, err := bolt.Open(dbPath, 0666, nil)
	if err != nil {
		return 0, fmt.Errorf("failed to open database: %v", err)
	}
	defer db.Close()
	var result int64
	err = db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(sha1BucketName)
		if b == nil {
			return fmt.Errorf("bucket '%s' not found", string(sha1BucketName))
		}
		bresult := b.Get(hash)
		if bresult == nil {
			return nil
		}
		var n int
		result, n = binary.Varint(bresult)
		if n <= 0 {
			return fmt.Errorf("invalid data")
		}
		return nil
	})
	return result, err
}

type lineData struct {
	SHA1Hash []byte
	Times    int64
}

func parseLine(line string) (*lineData, error) {
	if line == "" {
		return nil, errors.New("line is empty")
	}
	parts := strings.Split(strings.TrimSpace(line), ":")
	if len(parts) > 2 {
		return nil, fmt.Errorf("line '%s' has invalid format", line)
	}
	hash, err := hex.DecodeString(parts[0])
	if err != nil {
		return nil, fmt.Errorf("failed to parse sha1 hash '%s': %v", parts[0], err)
	}
	if len(hash) != sha1.Size {
		return nil, fmt.Errorf("failed to parse sha1 hash '%s': invalid hash size %d", parts[0], len(hash))
	}
	data := lineData{hash, 1}
	if len(parts) == 2 {
		data.Times, err = strconv.ParseInt(parts[1], 10, 64)
		if err != nil {
			return nil, fmt.Errorf("failed to parse number of password usages: %v", err)
		}
	}
	return &data, nil
}

func dumpLinesData(db *bolt.DB, linesData []*lineData) error {
	return db.Update(func(tx *bolt.Tx) error {
		for _, lineData := range linesData {
			bucket := tx.Bucket(sha1BucketName)
			if bucket == nil {
				return fmt.Errorf("failed to dump hashes: no bucket %s", string(sha1BucketName))
			}
			timesBuf := make([]byte, binary.MaxVarintLen64)
			_ = binary.PutVarint(timesBuf, lineData.Times)
			err := bucket.Put(lineData.SHA1Hash, timesBuf)
			if err != nil {
				return fmt.Errorf("failed to dump hashes: %v", err)
			}
		}
		return nil
	})
}

// ConvertDatabase is ...
func ConvertDatabase(
	rawFilePath,
	dbPath string,
	onProgress func(int),
	onWarn func(error)) error {
	file, err := os.Open(rawFilePath)
	if err != nil {
		return fmt.Errorf("failed to open raw file '%s': %v", rawFilePath, err)
	}
	defer file.Close()
	db, err := bolt.Open(dbPath, 0600, nil)
	if err != nil {
		return fmt.Errorf("failed to open database file '%s': %v", dbPath, err)
	}
	defer db.Close()
	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(sha1BucketName)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("failed to create bucket: %v", err)
	}
	bfile := bufio.NewReader(file)
	const capacity = 1000
	linesData := make([]*lineData, 0, capacity)
	line, fileErr := bfile.ReadString('\n')
	i := 1
	for fileErr == nil {
		line = strings.TrimSpace(line)
		lineData0, err := parseLine(line)
		if err == nil {
			linesData = append(linesData, lineData0)
			if len(linesData) >= capacity {
				err = dumpLinesData(db, linesData)
				if err != nil {
					return err
				}
				linesData = make([]*lineData, 0, capacity)
			}
		} else {
			onWarn(fmt.Errorf("failed to parse line %d: %v", i, err))
		}
		onProgress(i)
		line, fileErr = bfile.ReadString('\n')
		i++
	}
	if fileErr != io.EOF {
		return fmt.Errorf("failed to read file '%s': %v", rawFilePath, err)
	}
	line = strings.TrimSpace(line)
	if line != "" {
		lineData0, err := parseLine(line)
		if err != nil {
			return fmt.Errorf("failed to parse line %d: %v", i, err)
		}
		linesData = append(linesData, lineData0)
	}
	return dumpLinesData(db, linesData)
}
