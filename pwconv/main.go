package main

import (
	"fmt"
	"os"
	"time"

	"bitbucket.org/yekimov/pwned"
)

func onWarn(err error) {
	fmt.Println("WARNING:", err)
}

func main() {
	if len(os.Args) < 3 {
		fmt.Println("Usage: pwconv <inFile> <outFile>")
		os.Exit(1)
	}
	t := time.Now()
	err := pwned.ConvertDatabase(os.Args[1], os.Args[2], func(i int) {
		if i%1000 == 0 {
			t0 := time.Now()
			if t0.Sub(t) >= 2*time.Second {
				fmt.Printf("%11d lines handled.\n", i)
				t = t0
			}
		}
	}, onWarn)
	if err != nil {
		fmt.Println("ERROR:", err)
	}
}
